/*
 * File: montecarlo_pi.c
 *
 * Purpose: compute an estimate of the value of Pi using
 *      the monte carlo method
 *
 * To compile:
 *
 * gcc -g -wall -ln montecarlo_pi.c -o montecarlo_pi.exe
 *
 * NOTES:
 * argc = argument count (# of arguments, including the command itself) 
 * argv = argument vector (1-D array of strings)
 *
 * if we run the program using 
 *
 * ./montecarlo_pi.exe <number_of_iteration>
 *
 * then argc = 2, argv[0] = the executable file,
 *
 *and argv[1] = number of iterations
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

int main(int argc, char** argv ) {
    double x, y; // coordinates of each point
    int total_points = atoi( argv[1] ); // convert string to int
    int i; //counter variable for for loop
    int points_in_circle = 0;
    double z; //test value to see if point (x,y) is in circle
    double pi = 0;

    srand( time(NULL) ); // seed random number generator using system clock

    //generate randomly located points in a 2x2 square
    //in which a unit circle (of radius 1) has been inscribed
    for (i=0; i<total_points; i++){

        //get random point(x,y) inside the first quadrant of the square
        x= 2.0 * (double) rand()/RAND_MAX - 1.0;//-1.0 <= x < 1.0
        y= 2.0 * (double) rand()/RAND_MAX - 1.0;//-1.0 <= y < 1.0
        //next we'll need to see if the point (x,y) 
        //lies within the unit circle inscribed in the square
        z=sqrt(x*x + y*y);
        //if sqrt(x^2 + y^2) <= 1, then we count that
        //point as being inside the first quadrant of unit circle
        //if z > 1 the point is outside of the circle
        
        if(z <= 1){
            points_in_circle++;
        }//end if 
    }//end for 
    /*
    * If we let P refer to ratio of points inside the circle
    * to the total number of points, then if the number of points
    * is large enough, P approaches the area of a unit circle 
    * (A = pi*r^2, with r = 1) divided by the area of the 2x2 square
    * in which the circle is inscribed (A = (2r)^2 = 4*r^2, r = 1).
    * This ratio evaluates to pi/4, which when multiplied by 4 
    * results in our Monte Carlo estimate of pi.
    */
    pi = ((double) points_in_circle/total_points) * 4.0;

    //output result
    printf("Pi: %f/n", pi);

    return 0;


}//end main
