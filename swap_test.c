// Peter Mize
/* NOTES: Consider drawing a swim lane diagram to keep track of pointers
 * Pointers are addresses
 */


/* preprocessor directives */
#include <stdio.h>

/* function prototypes */

void swap_attempt1(int, int);
void swap_attempt2(int*, int*);

int main()
{
	int x, y;

	printf("Enter the value of x and y\n");
	scanf("%d%d",&x,&y); // scanf stores the entered values at the 
                         // ADDRESSES of x and y 

	printf("\nIn function main - before calling swap_attempt1:\nx = %d\ny = %d\n", x, y);

    

	swap_attempt1(x, y); // pass (copies of) the values of x and y

	printf("\nIn function main - after swap_attempt1:\nx = %d\ny = %d\n", x, y);



	swap_attempt2(&x, &y); // pass ????

	printf("\nIn function main - after swap_attempt2:\nx = %d\ny = %d\n", x, y);


	return 0;
} // end main


/*
 * a and b are COPIES of the VALUES of x and y in main
 */
void swap_attempt1( int a, int b)
{
    int temp = a;
	
    printf("\tIn function swap_attempt1 - before swapping values: \n\ta = %d\n\tb = %d\n", a, b);   
    //temp = a; 
    //b = temp;
	a = b;
    b = temp;
    
	printf("\tIn function swap_attempt1 - after swapping values: \n\ta = %d\n\tb = %d\n", a, b);  
	
} 

/*
 * a and b are COPIES of the ADDRESSES of x and y in main
 */
void swap_attempt2( int* a_p, int* b_p)
{

    
    // swap the POINTERS (i.e. the ADDRESSES themselves) 
    //int* temp_p = a_p; //a temporary int  
	
    // swap the POINTEES (not the pointers)
    int temp; // a temporary int 
    temp = *a_p; // HERE *a_p is a copy of the copied addresses assigned to temp value. 

    printf("\tIn function swap_attempt2 - before swapping pointees: \n\t*a_p = %d\n\t*b_p = %d\n", *a_p, *b_p);   
	*a_p = *b_p;
    *b_p = temp;
    
	printf("\tIn function swap_attempt2 - after swapping pointees: \n\t*a_p = %d\n\t*b_p = %d\n", *a_p, *b_p);  
}

