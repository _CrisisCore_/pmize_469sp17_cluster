/*
 * Peter Mize
 * serial C program to demonstrate
 * function definitions and prototypes
 */

#include <stdio.h>

/* declare all function prototypes */
int square( int y );

/* main function begins program execution */
int main( void ) { 
    int x; //counter variable

    for (x=1; x<=10; x++){
       printf("%d ", square( x ) ); 
    }//end for

    puts(""); //prints an empty string with a newline character
    return 0;
}//end main
/*
 * make sure to define all functions whose 
 * prototypes were declared above
 */ 
int square(int y){
    return y*y;
}//end square
