/* preprocessor directives */
#include <stdio.h> //standard input/output header file

/* function prototypes if any, are declared before main */

int main( void ) {
  printf("Hello ");
  printf("World!\n");

  return 0;
}//end function main 
