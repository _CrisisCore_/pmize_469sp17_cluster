#!/bin/sh
### Job Name
#PBS -N pmize_hello
### Output files
#PBS -o mpi_hello.out
#PBS -e mpi_hello.err
### Specify Resources
#PBS -l nodes=3:ppn=2
#PBS -q default

###source /share/apps/Modules/3.2.10/init/sh

mpirun -np 6 -machinefile $PBS_NODEFILE ~/csci469/ch3/mpi_hello
